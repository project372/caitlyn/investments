﻿using Caitlyn.Investments.Domain.Abstractions.Interfaces;
using Caitlyn.Investments.Domain.Abstractions.Types;

namespace Caitlyn.Investments.Domain.Models;

public class AssetInvestmentQuota : IInvestmentQuota
{
    public NonZeroInteger32 Quantity { get; init; } = new();
    public NonZeroUnsignedDecimal InitialPrice { get; init; } = new();
    public UnsignedDecimal ActualPrice { get; init; } = new();
    public  UnsignedDecimal Earnings { get; init; } = new();
    public  NonZeroUnsignedDecimal InitialValue => Quantity.GetAbsoluteValue() * InitialPrice;
    public UnsignedDecimal FinalValue => Quantity.GetAbsoluteValue() * ActualPrice;
    public decimal CashFlow => Quantity.Value * ActualPrice.Value;

    public bool IsSelling() => Quantity < 0;


}