﻿using Caitlyn.Investments.Domain.Abstractions.Interfaces;
using Caitlyn.Investments.Domain.Abstractions.Types;

namespace Caitlyn.Investments.Domain.Models;

public class InvestmentQuota : IInvestmentQuota
{
    public NonZeroUnsignedDecimal InitialValue { get; init; }
    public UnsignedDecimal FinalValue { get; init; }
    public UnsignedDecimal Earnings { get; init; }
    public decimal CashFlow { get; init; }
}