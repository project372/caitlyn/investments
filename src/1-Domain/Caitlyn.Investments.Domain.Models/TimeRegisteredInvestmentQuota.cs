﻿namespace Caitlyn.Investments.Domain.Models;

public class TimeRegisteredInvestmentQuota : AssetInvestmentQuota
{
    public DateTime Date { get; init; }
}