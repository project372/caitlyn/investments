﻿using Caitlyn.Investments.CrossCutting.Errors.ErrorCodes.Domain.Abstractions;

namespace Caitlyn.Investments.Domain.Abstractions.Types;

public record NonZeroUnsignedDecimal : UnsignedDecimal
{
    public static NonZeroUnsignedDecimal operator *(int a, NonZeroUnsignedDecimal b) => new () {Value = a * b.Value};

    public override IEnumerable<string> Validate()
    {
        return Value == 0 ? new[] { TypesErrorCodes.NonZeroUnsignedDecimalWithZeroValue } : base.Validate();
    }
}