﻿using Caitlyn.Investments.CrossCutting.Errors.ErrorCodes.Domain.Abstractions;
using Caitlyn.Investments.Domain.Abstractions.Interfaces;

namespace Caitlyn.Investments.Domain.Abstractions.Types;

public record NonZeroInteger32 : IValidatable
{
    public int Value { get; init; }

    public int GetAbsoluteValue() => Math.Abs(Value);

    public static bool operator <(NonZeroInteger32 a, int b) => a.Value < b;
    public static bool operator >(NonZeroInteger32 a, int b) => a.Value > b;

    public IEnumerable<string> Validate() => Value == 0 ? new[] { TypesErrorCodes.NonZeroIntegerWithZeroValue } : Array.Empty<string>();
}