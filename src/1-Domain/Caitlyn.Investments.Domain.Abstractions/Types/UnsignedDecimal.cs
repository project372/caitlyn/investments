﻿using Caitlyn.Investments.CrossCutting.Errors.ErrorCodes.Domain.Abstractions;
using Caitlyn.Investments.Domain.Abstractions.Interfaces;

namespace Caitlyn.Investments.Domain.Abstractions.Types;

public record UnsignedDecimal : IValidatable
{
    public decimal Value { get; init; }

    public static bool operator ==(UnsignedDecimal a, decimal b) => a.Value == b;
    public static bool operator ==(decimal a, UnsignedDecimal b) => a == b.Value;

    public static bool operator !=(UnsignedDecimal a, decimal b) => a.Value != b;
    public static bool operator !=(decimal a, UnsignedDecimal b) => a != b.Value;

    public static UnsignedDecimal operator *(int a, UnsignedDecimal b) => new () {Value = a * b.Value};

    public virtual IEnumerable<string> Validate()
    {
        return Value < 0 ? new[] { TypesErrorCodes.UnsignedDecimalWithNegativeValue } : Array.Empty<string>();
    }
}