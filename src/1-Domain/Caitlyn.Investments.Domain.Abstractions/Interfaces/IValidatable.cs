﻿namespace Caitlyn.Investments.Domain.Abstractions.Interfaces;

public interface IValidatable
{
    IEnumerable<string> Validate();
}