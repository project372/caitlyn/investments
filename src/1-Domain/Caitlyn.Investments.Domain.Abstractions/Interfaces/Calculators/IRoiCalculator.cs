﻿namespace Caitlyn.Investments.Domain.Abstractions.Interfaces.Calculators;

public interface IRoiCalculator
{
    float CalculateRoi(IInvestmentQuota quota);
}