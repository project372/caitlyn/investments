﻿using Caitlyn.Investments.Domain.Abstractions.Types;

namespace Caitlyn.Investments.Domain.Abstractions.Interfaces;

public interface IInvestmentQuota 
{
    public  NonZeroUnsignedDecimal InitialValue { get; }
    public UnsignedDecimal FinalValue { get; }
    public UnsignedDecimal Earnings { get; }
    public decimal CashFlow { get; }
    
}