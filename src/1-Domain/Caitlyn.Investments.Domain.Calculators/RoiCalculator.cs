﻿using Caitlyn.Investments.Domain.Abstractions.Interfaces;
using Caitlyn.Investments.Domain.Abstractions.Interfaces.Calculators;

namespace Caitlyn.Investments.Domain.Calculators;

public class RoiCalculator : IRoiCalculator
{
    public float CalculateRoi(IInvestmentQuota quota)
    {
        var value = (quota.Earnings.Value + quota.FinalValue.Value + quota.CashFlow- quota.InitialValue.Value) / quota.InitialValue.Value;
        return (float)value;
    }
}