﻿namespace Caitlyn.Investments.CrossCutting.Errors.ErrorCodes.Domain.Abstractions;

public static class TypesErrorCodes
{
    public const string UnsignedDecimalWithNegativeValue = nameof(UnsignedDecimalWithNegativeValue);
    public const string NonZeroUnsignedDecimalWithZeroValue = nameof(NonZeroUnsignedDecimalWithZeroValue);
    public const string NonZeroIntegerWithZeroValue = nameof(NonZeroIntegerWithZeroValue);
    
}