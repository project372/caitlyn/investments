﻿namespace Caitlyn.Investments.CrossCutting.Errors;

public class CustomException: Exception
{
    protected CustomException(string message, Exception? innerException = null) : base(message, innerException)
    {
        
    }
}