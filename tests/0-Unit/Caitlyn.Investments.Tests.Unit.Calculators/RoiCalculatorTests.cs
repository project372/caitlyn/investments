﻿using Caitlyn.Investments.Domain.Abstractions.Types;
using Caitlyn.Investments.Domain.Calculators;
using Caitlyn.Investments.Domain.Models;
using Xunit;

namespace Caitlyn.Investments.Tests.Unit.Calculators;

public class RoiCalculatorTests
{
    [Fact]
    public void GetRoiAsyncShouldReturnTheExpectedRoi()
    {
        var initialValue = new NonZeroUnsignedDecimal { Value = 10000 };
        var finalValue = new UnsignedDecimal { Value = 13350 };
        var earnings = new UnsignedDecimal { Value = 500 };
        var cashflow = -150m;

        var quota = new InvestmentQuota()
        {
            Earnings = earnings,
            InitialValue = initialValue,
            FinalValue = finalValue,
            CashFlow = cashflow
        };

        var sut = new RoiCalculator();
        var roi = sut.CalculateRoi(quota);
        
        Assert.Equal(.37f, roi);
    }
}